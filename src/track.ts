export class Track {
  balls: number[] = [];

  constructor(private capacity: number) {
  }

  addToTrack(ballNumber: number) {
    const output: ITrackOutput = {};
    if (this.balls.length < this.capacity) {
      this.balls.push(ballNumber);
    } else {
      output.enqueue = this.balls.reverse();
      output.passOn = ballNumber;
      this.balls = [];
    }
    return output;
  }
}

export interface ITrackOutput {
  /** the balls to be returned to the queue */
  enqueue?: number[];
  /** the ball to pass on to the next track */
  passOn?: number;
}
