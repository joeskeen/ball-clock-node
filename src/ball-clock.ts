import { Track, ITrackOutput } from './track';

export class BallClock {
  minutesTrack = new Track(4);
  fiveMinutesTrack = new Track(11);
  hoursTrack = new Track(11);
  mainQueue: number[] = [];

  constructor(private capacity: number) {
    if (capacity < 27 || capacity > 127)
      throw new Error('Capacity must be between 27 and 127.');

    for (let i = 1; i <= capacity; i++) {
      this.mainQueue.push(i);
    }
  }

  get tracks() {
    return [
      this.minutesTrack,
      this.fiveMinutesTrack,
      this.hoursTrack
    ];
  }

  get status() {
    return {
      Min: this.minutesTrack.balls,
      FiveMin: this.fiveMinutesTrack.balls,
      Hour: this.hoursTrack.balls,
      Main: this.mainQueue
    };
  }

  get isReset() {
    if (this.mainQueue.length !== this.capacity)
      return false;

    for (let i = 1; i <= this.mainQueue.length; i++) {
      if (this.mainQueue[i - 1] !== i)
        return false;
    }

    return true;
  }

  tick() {
    const ball = this.mainQueue.shift();
    const tracks = this.tracks;
    let output: ITrackOutput = { passOn: ball };
    let i = 0;
    do {
      output = tracks[i].addToTrack(output.passOn);
      if (output.enqueue) {
        this.mainQueue = this.mainQueue.concat(output.enqueue);
      }
      i++;
    } while (output.passOn && i < tracks.length);
    if (output.passOn) {
      this.mainQueue.push(output.passOn);
    }
  }
}

export interface IClockStatus {
  Min: number[],
  FiveMin: number[],
  Hour: number[],
  Main: number[]
}