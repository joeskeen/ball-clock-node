#!/usr/bin/env node

/// <reference types="node" />
import { daysToReset, statusAfter } from './index';

const args = process.argv.slice(2);
if (!args.length || args.length > 2)
  throw new Error('Invalid number of parameters. Must pass one or two numbers.');

const balls = Number(args[0]);
if (isNaN(balls))
  throw new Error('Arguments must be numeric.');

if (args.length === 1) {
  const days = daysToReset(balls);
  console.log(`${balls} balls cycle after ${days} days.`);
} else {
  const minutes = Number(args[0]);
  if (isNaN(minutes))
    throw new Error('Arguments must be numeric.');

  const status = statusAfter(balls, minutes);
  console.log(JSON.stringify(status));
}