import { BallClock } from './ball-clock';

export function daysToReset(balls: number) {
  const clock = new BallClock(balls);
  let i = 0;
  do {
    clock.tick();
    i++;
  } while(!clock.isReset);
  const minutesInADay = 60 * 24;
  const days = Math.floor(i / minutesInADay);
  return days;
}

export function statusAfter(balls: number, minutes: number) {
  const clock = new BallClock(balls);
  for (let i = 0; i < minutes; i++) {
    clock.tick();
  }
  return clock.status;
}
