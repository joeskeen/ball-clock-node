#!/usr/bin/env node
"use strict";
const index_1 = require("./index");
const args = process.argv.slice(2);
if (!args.length || args.length > 2)
    throw new Error('Invalid number of parameters. Must pass one or two numbers.');
const balls = Number(args[0]);
if (isNaN(balls))
    throw new Error('Arguments must be numeric.');
if (args.length === 1) {
    index_1.daysToReset(balls);
}
else {
    const minutes = Number(args[0]);
    if (isNaN(minutes))
        throw new Error('Arguments must be numeric.');
    index_1.printStatusAfter(balls, minutes);
}
