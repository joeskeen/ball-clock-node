"use strict";
class Track {
    constructor(capacity) {
        this.capacity = capacity;
        this.balls = [];
    }
    addToTrack(ballNumber) {
        const output = {};
        if (this.balls.length < this.capacity) {
            this.balls.push(ballNumber);
        }
        else {
            output.enqueue = this.balls.reverse();
            output.passOn = ballNumber;
            this.balls = [];
        }
        return output;
    }
}
exports.Track = Track;
