"use strict";
const ball_clock_1 = require("./ball-clock");
function daysToReset(balls) {
    const clock = new ball_clock_1.BallClock(balls);
    let i = 0;
    do {
        clock.tick();
        i++;
    } while (!clock.isReset);
    const minutesInADay = 60 * 24;
    const days = Math.floor(i / minutesInADay);
    console.log(`${balls} balls cycle after ${days} days.`);
}
exports.daysToReset = daysToReset;
function printStatusAfter(balls, minutes) {
    const clock = new ball_clock_1.BallClock(balls);
    for (let i = 0; i < minutes; i++) {
        clock.tick();
    }
    console.log(JSON.stringify(clock.status));
}
exports.printStatusAfter = printStatusAfter;
