import { expect } from 'chai';
import { daysToReset } from '../src/index';

describe('The ball clock', () => {
  describe('with 30 balls', () => {
    it('should reset after 15 days', () => {
      const days = daysToReset(30);
      expect(days).to.equal(15);
    });
  });

  describe('with 45 balls', () => {
    it('should reset after 378 days', () => {
      const days = daysToReset(45);
      expect(days).to.equal(378);
    });
  });
});
