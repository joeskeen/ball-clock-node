import { expect } from 'chai';
import { IClockStatus } from '../src/ball-clock';
import { statusAfter } from '../src/index';

describe('The ball clock with 30 balls, after running for 325 minutes', () => {
  let status: IClockStatus;
  before(() => {
    status = statusAfter(30, 325);
  });

  it('should have no balls in the Min track', () => {
    expect(status.Min.length).to.equal(0);
  });
  it('should have balls [22,13,25,3,7] in the FiveMin track', () => {
    expect(status.FiveMin).to.be.deep.equal([22, 13, 25, 3, 7]);
  });
  it('should have balls [6,12,17,4,15] in the Hour track', () => {
    expect(status.Hour).to.be.deep.equal([6, 12, 17, 4, 15]);
  });
  it('should have balls [11,5,26,18,2,30,19,8,24,10,29,20,16,21,28,1,23,14,27,9] in the Main track', () => {
    expect(status.Main).to.be.deep.equal([11, 5, 26, 18, 2, 30, 19, 8, 24, 10, 29, 20, 16, 21, 28, 1, 23, 14, 27, 9]);
  });
});
