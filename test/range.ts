import { expect } from 'chai';
import { BallClock } from '../src/ball-clock';

describe('The ball clock', () => {
  it('should reject ball counts less than 27', () => {
    expect(() => new BallClock(26)).to.throw();
  });
  it('should reject ball counts greater than 127', () => {
    expect(() => new BallClock(128)).to.throw();
  });
  it('should accept ball count of 27', () => {
    expect(() => new BallClock(27)).not.to.throw();
  });
  it('should accept ball count of 127', () => {
    expect(() => new BallClock(127)).not.to.throw();
  });
});
