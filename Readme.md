# `ball-clock`

A ball clock simulation written in Node.js.

Clock supports two modes of computation:

The first mode takes a single parameter specifying the number of balls and reports the
number of balls given in the input and the number of days (24-hour periods) which elapse
before the clock returns to its initial ordering.

```bash
$ ball-clock 30
30 balls cycle after 15 days.
$ ball-clock 45
45 balls cycle after 378 days.
```

The second mode takes two parameters, the number of balls and the number of minutes to
run for. If the number of minutes is specified, the clock must run to the number of minutes
and report the state of the tracks at that point in a JSON format.

```bash
$ ball-clock 30 325
{"Min":[],"FiveMin":[22,13,25,3,7],"Hour":[6,12,17,4,15],"Main":[11,5,26,18,2,30,19,8,24,10,29,20,16,21,28,1,23,14,27,9]}
```

## Set up

To run this, you must have NodeJS 6+ installed.  You can then run `npm link` from the root
of this repository to wire up the `ball-clock` command.

This program was written using TypeScript.  The original source files can be found in the
`src` directory.

You can run the tests (written with Mocha and Chai) by running `npm test`.